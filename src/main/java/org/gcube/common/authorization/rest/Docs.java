package org.gcube.common.authorization.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Francesco Mangiacrapa (ISTI-CNR)
 * @author Luca Frosini (ISTI-CNR)
 */
@Path("docs")
public class Docs {

	private static Logger logger = LoggerFactory.getLogger(Docs.class);

	@GET
	@Path("{any: .*}")
	public InputStream toDoc(@Context HttpServletRequest req) throws WebApplicationException {
		
		String pathInfo = req.getPathInfo();
		try {
			
			if (pathInfo.endsWith("/docs")) {
				pathInfo += "/index.html";
			}
			
			if (pathInfo.endsWith("/docs/")) {
				pathInfo += "index.html";
			}

			logger.info("redirecting to {}", pathInfo);

			String realPath = req.getServletContext().getRealPath(pathInfo);
			return new FileInputStream(new File(realPath));

		} catch (Exception e) {
			throw new InternalServerErrorException(e);
		}
	}

}


package org.gcube.common.authorization.rest;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.common.scope.api.ScopeProvider;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Path("goat")
public class Goat extends BaseREST {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getOldToken() {
		setCalledMethod("GET /goat");
		try {
			String oldToken = SecurityTokenProvider.instance.get();
			if(oldToken==null) {
				String context = ScopeProvider.instance.get();
				String username = AuthorizationProvider.instance.get().getClient().getId();
				UserInfo userInfo = new UserInfo(username, new ArrayList<>());
				oldToken = authorizationService().generateUserToken(userInfo, context);
			}
			return oldToken;
		} catch(WebApplicationException e) {
			throw e;
		} catch(Exception e) {
			throw new InternalServerErrorException(e);
		}
	}
	
}

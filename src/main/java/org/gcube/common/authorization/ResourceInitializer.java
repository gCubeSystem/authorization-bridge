package org.gcube.common.authorization;

import javax.ws.rs.ApplicationPath;

import org.gcube.common.authorization.rest.Goat;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@ApplicationPath("/")
public class ResourceInitializer extends ResourceConfig {
	
	public ResourceInitializer() {
		packages(Goat.class.getPackage().toString());
	}
	
}
